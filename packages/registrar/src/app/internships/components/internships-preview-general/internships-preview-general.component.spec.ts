import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InternshipsPreviewGeneralComponent } from './internships-preview-general.component';

describe('InternshipsPreviewGeneralComponent', () => {
  let component: InternshipsPreviewGeneralComponent;
  let fixture: ComponentFixture<InternshipsPreviewGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InternshipsPreviewGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InternshipsPreviewGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
