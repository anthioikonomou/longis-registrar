import { Injectable } from "@angular/core";
import Dexie from 'dexie';

export class AppDB extends Dexie {

  constructor() {
    super('registrar');
    this.version(3).stores({
      UserQuery: '++id,entityType,hashCode,entitySet,dateAccessed'
    });
    this.on('populate', () => this.populate());
  }

  async populate() {
    //
  }

  async resetDatabase() {
    await this.populate();
  }

}

@Injectable({
  providedIn: 'root'
})
export class ApplicationDatabase {

  public readonly db = new AppDB();

  constructor() {
    //
  }

}
