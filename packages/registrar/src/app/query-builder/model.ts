export interface UserQuery {
  id?: number;
  name?: string;
  query?: any;
  description: string;
  entityType?: string;
  entitySet?: string;
  dateCreated?: Date;
  dateAccessed?: Date;
  hashCode?: string;
}