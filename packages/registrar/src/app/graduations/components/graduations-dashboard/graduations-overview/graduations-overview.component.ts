import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {DatePipe} from '@angular/common';
import {ConfigurationService} from '@universis/common';

@Component({
  selector: 'app-graduations-overview',
  templateUrl: './graduations-overview.component.html'
})
export class GraduationsOverviewComponent implements OnInit {

  public graduationEvent: any;

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) { }

  async ngOnInit() {

    const graduationEvent = await this._context.model('GraduationEvents')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('location,reportTemplates,studyPrograms($expand=studyLevel),attachmentTypes($expand=attachmentType)')
      .getItem();

    this.graduationEvent = graduationEvent;
  }

}
