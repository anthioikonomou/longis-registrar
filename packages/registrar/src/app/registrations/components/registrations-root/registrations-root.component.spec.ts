import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationsRootComponent } from './registrations-root.component';

describe('RegistrationsRootComponent', () => {
  let component: RegistrationsRootComponent;
  let fixture: ComponentFixture<RegistrationsRootComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationsRootComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationsRootComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
