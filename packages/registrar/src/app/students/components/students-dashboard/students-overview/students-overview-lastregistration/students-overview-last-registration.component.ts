import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';
import {RegistrationsService} from '../../../../../registrations/services/registrations.service';
import {
  ActiveDepartmentService
} from '../../../../../registrar-shared/services/activeDepartmentService.service';
import {TranslateService} from '@ngx-translate/core';
import {DIALOG_BUTTONS, ModalService, ToastService} from '@universis/common';

@Component({
  selector: 'app-students-overview-lastregistration',
  templateUrl: './students-overview-last-registration.component.html',
  styleUrls: ['./students-overview-last-registration.component.scss']
})
export class StudentsOverviewLastRegistrationComponent implements OnInit, OnDestroy {
  public lastRegistration: any;
  public allowNewRegistration = true;
  public activeDepartment: any;
  public isLoading = true;
  @Input() studentId: number;
  private subscription: Subscription;
  public department: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private registrationsService: RegistrationsService,
              private router: Router,
              private _activeDepartmentService: ActiveDepartmentService,
              private _translateService: TranslateService,
              private _toastService: ToastService,
              private _modalService: ModalService) {
  }

  async ngOnInit() {
    try {
      this.activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this.department = await this._context.model('LocalDepartments')
        .where('id').equal(this.activeDepartment && this.activeDepartment.id)
        .select('currentYear', 'currentPeriod')
        .getItem();
      if (this.department) {
        Object.assign(this.department,
          {currentPeriodTranslated: this._translateService.instant('Periods.' + this.department.currentPeriod.alternateName)});
      }

      this.subscription = this._activatedRoute.params.subscribe(async (params) => {
        this.studentId = params.id;
        try {
          this.lastRegistration = await this._context.model('Students/' + this.studentId + '/LastPeriodRegistration')
            .asQueryable()
            // tslint:disable-next-line:max-line-length
            .expand('documents($orderby=dateCreated desc;$top=1),classes($select=registration,sum(ects) as ects,count(id) as total;$groupby=registration)')
            .getItem();
          const studentStatus = await this._context.model('Students').where('id').equal(this.studentId)
            .select('studentStatus').expand('studentStatus').getItem();
          if (studentStatus.studentStatus.alternateName !== 'active') {
            this.allowNewRegistration = false;
          } else if (this.lastRegistration) {
            // tslint:disable-next-line:max-line-length
            if (this.lastRegistration.registrationYear && this.lastRegistration.registrationYear.id === this.department.currentYear.id &&
              // tslint:disable-next-line:max-line-length
              this.lastRegistration.registrationPeriod && this.lastRegistration.registrationPeriod.id >= this.department.currentPeriod.id) {
              this.allowNewRegistration = false;
            }
          }
          this.isLoading = false;
        } catch (error) {
          this.isLoading = false;
          console.log(error);
        }
      });
    } catch (error) {
      this.isLoading = false;
      console.log(error);
    }
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  async createRegistration() {
    const title = this._translateService.instant('Students.Registration.NewModalTitle');
    const message = this._translateService.instant('Students.Registration.NewModalMsg');
    const dialogResult = await this._modalService.showDialog(
      title,
      message, DIALOG_BUTTONS.YesNo);
    if (dialogResult === 'no') {
      return;
    }
    // note: Department does not be to be refetched here. The action is only available
    // if the student does not have a registration for what is consider the currentYear and currentPeriod.
    if (!(this.department && this.department.currentYear && this.department.currentPeriod)) {
      return Promise.resolve();
    }
    const newRegistration = {
      student: this.studentId,
      registrationYear: this.department.currentYear,
      registrationPeriod: this.department.currentPeriod,
      registrationDate: new Date(),
      status: {
        alternateName: 'open'
      }
    };

    const registrationResult = await this.registrationsService.saveRegistration(newRegistration);
    if (registrationResult && registrationResult.validationResult) {
      if (registrationResult.validationResult.success) {
        this.lastRegistration = await this._context.model('StudentPeriodRegistrations')
          .where('id').equal(registrationResult.id)
          .getItem();
        this.allowNewRegistration = false;
        this._toastService.show(
          this._translateService.instant('Students.Registration.SuccessTitle'),
          this._translateService.instant('Students.Registration.SuccessMsg')
        );
      } else {
        this._modalService.showErrorDialog(this._translateService.instant('Registrations.NoSuccessModal.Title'),
          this._translateService.instant('Registrations.NoSuccessModal.Message'));
      }
    } else {
      this._modalService.showErrorDialog(this._translateService.instant('Registrations.NoSuccessModal.Title'),
        this._translateService.instant('Registrations.NoSuccessModal.Message'));
    }
  }

}
