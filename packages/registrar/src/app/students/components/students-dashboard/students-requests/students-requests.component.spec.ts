import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsRequestsComponent } from './students-requests.component';

describe('StudentsRequestsComponent', () => {
  let component: StudentsRequestsComponent;
  let fixture: ComponentFixture<StudentsRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
