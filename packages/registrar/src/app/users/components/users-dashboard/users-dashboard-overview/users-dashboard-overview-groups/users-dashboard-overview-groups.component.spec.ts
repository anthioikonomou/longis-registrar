import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersDashboardOverviewGroupsComponent } from './users-dashboard-overview-groups.component';

describe('UsersDashboardOverviewGroupsComponent', () => {
  let component: UsersDashboardOverviewGroupsComponent;
  let fixture: ComponentFixture<UsersDashboardOverviewGroupsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersDashboardOverviewGroupsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersDashboardOverviewGroupsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
