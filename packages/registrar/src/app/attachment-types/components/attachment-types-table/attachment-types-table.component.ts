import { Component, OnInit } from '@angular/core';
import * as AttachmentTypes_LIST_CONFIG from '../attachment-types-table/attachment-types-table.config.list.json';

@Component({
  selector: 'app-attachment-types-table',
  templateUrl: './attachment-types-table.component.html'
})
export class AttachmentTypesTableComponent implements OnInit {
  public readonly config = AttachmentTypes_LIST_CONFIG;
  constructor() { }

  ngOnInit() {
  }

}
