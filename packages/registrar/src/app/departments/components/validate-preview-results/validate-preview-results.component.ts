import {Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import * as VALIDATE_RESULTS_LIST_CONFIG from './validate-results-table.config.list.json';
import {Observable, Subscription} from 'rxjs';
import {AdvancedSearchFormComponent} from '@universis/ngx-tables';
import {ActivatedTableService} from '@universis/ngx-tables';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {ErrorService, LoadingService, ModalService, UserActivityService} from '@universis/common';
import {ClientDataQueryable} from '@themost/client';
import {TranslateService} from '@ngx-translate/core';


@Component({
  selector: 'app-validate-preview-results',
  templateUrl: './validate-preview-results.component.html'
})
export class ValidatePreviewResultsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>VALIDATE_RESULTS_LIST_CONFIG;
  @ViewChild('students') students: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  public recordsTotal: any;
  private dataSubscription: Subscription;

  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  private department: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _context: AngularDataContext,
              private _loadingService: LoadingService,
              private _modalService: ModalService,
              private _errorService: ErrorService,
              private _userActivityService: UserActivityService,
              private _translateService: TranslateService
  ) {
  }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.department = this._activatedRoute.snapshot.data.department.id;
      this._activatedTable.activeTable = this.students;
      this.students.query = this._context.model('GraduationRuleValidateResults')
        .asQueryable()
         .prepare();
      this.students.config = AdvancedTableConfiguration.cast(VALIDATE_RESULTS_LIST_CONFIG);
      this.students.fetch();
      // declare model for advance search filter criteria
      this.students.config.model = 'GraduationRuleValidateResults';
      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.students.fetch(true);
        }
      });

      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
        if (data.tableConfiguration) {
          this.students.config = data.tableConfiguration;
          this.students.ngOnInit();
        }
        if (data.searchConfiguration) {
          this.search.form =  Object.assign(data.searchConfiguration, {department: this.department});
          this.search.ngOnInit();
        }
      });

      // save user activity for validation results list
      return this._userActivityService.setItem({
        category: this._translateService.instant('Sidebar.CheckGraduationRules'),
        description: this._translateService.instant('Rules.Results'),
        url: window.location.hash.substring(1),
        dateCreated: new Date()
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  async getSelectedItems() {
    let items = [];
    if (this.students && this.students.lastQuery) {
      const lastQuery: ClientDataQueryable = this.students.lastQuery;
      // search for document attributes (if table has published column)

      if (lastQuery != null) {
        if (this.students.smartSelect) {
          // get items
          const selectArguments = ['student/id as id'];

          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.students.unselected && this.students.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.students.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.students.selected.map((item) => {
            return {
              id: item.id
            };
          });
        }
      }
    }
    return items;
  }

}
