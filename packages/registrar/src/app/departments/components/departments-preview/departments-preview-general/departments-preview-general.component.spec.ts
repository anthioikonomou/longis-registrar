import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentsPreviewGeneralComponent } from './departments-preview-general.component';

describe('DepartmentsPreviewGeneralComponent', () => {
  let component: DepartmentsPreviewGeneralComponent;
  let fixture: ComponentFixture<DepartmentsPreviewGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentsPreviewGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentsPreviewGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
