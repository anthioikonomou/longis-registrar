import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-enrollment-event-overview-documents',
  templateUrl: './event-overview-documents.component.html'
})
export class EnrollmentEventOverviewDocumentsComponent implements OnInit {

  @Input() public enrollmentEvent: any;

  constructor() { }

  ngOnInit() {

  }

}
